#!/bin/sh

#Significant fixes:
# lcgeo: xmlcomment
# CEDViewer: python3
# MarlinReco: nullptr
# LCTuple: prot
# DDMarlinPandora: plug
# LCFIPlus: singleton

#-DCMAKE_BUILD_TYPE=Debug

. ./setup.sh

mkdir -p inst
mkdir -p build
cd build

# acas-sss branched from v02-02 config gitignore
#ILDConfig

# master as of 2021-02-27
#SiDPerformance

#rm -rf clhep
#mkdir clhep
#cd clhep
#cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_C_COMPILER=/usr/local/gcc/bin/gcc -DCMAKE_CXX_COMPILER=/usr/local/gcc/bin/g++ -DCLHEP_BUILD_CXXSTD=-std=c++17 /home/sss/atlas/extern/clhep/CLHEP
#make -j6 install 2>&1|tee log
#cd ..

# acas-sss branched from v02-16-01 (origin v02-16-sss): gitignore warn rootcint
rm -rf LCIO
mkdir LCIO
cd LCIO
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 -DBUILD_ROOTDICT=True ../../LCIO
make -j6 install 2>&1|tee log
cd ..

# acas-sss branched from v01-15 (origin sss): warn
rm -rf DD4hep
mkdir DD4hep
cd DD4hep
# No DDCAD --- runs into problems with assimp.
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 -DDD4HEP_USE_GEANT4=True -DDD4HEP_USE_XERCESC=True -DDD4HEP_USE_LCIO=True -DDD4HEP_BUILD_PACKAGES="DDRec DDDetectors DDCond DDAlign DDDigi DDG4 DDEve UtilityApps" ../../DD4hep 2>&1|tee log-cmake
make -j6 install 2>&1 | tee log
cd ..

# acas-sss branched from v00-16-06 (origin sss): warn[in master] gitignore xmlcomment[merging]
rm -rf lcgeo
mkdir lcgeo
cd lcgeo
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  -DDD4HEP_USE_GEANT4=True   ../../lcgeo/
make -j6 install 2>&1|tee log
cd ..

# acas-sss branched from v01-06-01 (origin sss): cmake gitignore rootcint
rm -rf iLCUtil
mkdir iLCUtil
cd iLCUtil
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 -DINSTALL_DOC=OFF    ../../iLCUtil
make -j6 install 2>&1|tee log
cd ..

 
# acas-sss branched from nightly-20200201 (origin sss): warn release-notes gitignore cmake
rm -rf GEAR
mkdir GEAR
cd GEAR
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  -DGEAR_TGEO=True    ../../GEAR
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from nightly-20200201 (origin sss): cmake warn gitignore
rm -rf CondDBMySQL
mkdir CondDBMySQL
cd CondDBMySQL
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17    ../../CondDBMySQL
make -j6 install 2>&1 | tee log
cd ..


# acas-sss branched from nightly-20200201 (origin sss): warn gitignore
rm -rf LCCD
mkdir LCCD
cd LCCD
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17    ../../LCCD
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from nightly-20200201 (origin sss): LibDeps warn gitignore
rm -rf CED
mkdir CED
cd CED
# FIXME: CED_SERVER off due to missing opengl/glut
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17   -DOpenGL_GL_PREFERENCE=GLVND -DCED_SERVER=OFF   ../../CED
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from nightly-20200201 (origin sss): libdeps gitignore warn
rm -rf RAIDA
mkdir RAIDA
cd RAIDA
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17    ../../RAIDA
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v01-17-01 (origin sss): warn
rm -rf Marlin
mkdir Marlin
cd Marlin
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17    ../../Marlin/
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from nightly-20200201 (origin sss): warn gitignore
rm -rf MarlinUtil
mkdir MarlinUtil
cd MarlinUtil
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  ../../MarlinUtil
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v01-17-01 (origin sss): warn[merging] gitignore python3[merging]
rm -rf CEDViewer
mkdir CEDViewer
cd CEDViewer
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  ../../CEDViewer
make -j6 install 2>&1|tee log
cd ..


# v00-06
rm -rf MarlinDD4hep
mkdir MarlinDD4hep
cd MarlinDD4hep
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../MarlinDD4hep/
make -j6 install 2>&1|tee log
cd ..


# v00-22-01
rm -rf Overlay
mkdir Overlay
cd Overlay
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  ../../Overlay
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v02-05 (origin sss): LibDeps warn gitignore
rm -rf KalTest
mkdir KalTest
cd KalTest
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  ../../KalTest
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from nightly-20200204 (origin sss): LibDeps warn
rm -rf KalDet
mkdir KalDet
cd KalDet
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../KalDet
make -j6 install 2>&1 | tee log
cd ..


# acas-sss branched from V02-02-01 (origin sss): cmake warn
rm -rf GeneralBrokenLines
mkdir GeneralBrokenLines
cd GeneralBrokenLines
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17    ../../GeneralBrokenLines
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v00-10 (origin sss); warn
rm -rf aidaTT
mkdir aidaTT
cd aidaTT
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  ../../aidaTT
make -j6 install 2>&1 | tee log
cd ..


# acas-sss branched from v01-06 (origin sss): warn cmake gitignore
rm -rf DDKalTest
mkdir DDKalTest
cd DDKalTest
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../DDKalTest
make -j6 install 2>&1 | tee log
cd ..


# acas-sss branched from v02-08 (origin sss): warn gitignore
rm -rf MarlinTrk
mkdir MarlinTrk
cd MarlinTrk
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  ../../MarlinTrk
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v01-10 (origin sss): LibDeps warn gitignore
rm -rf KiTrack
mkdir KiTrack
cd KiTrack
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../KiTrack
make -j6 install 2>&1 | tee log
cd ..


# acas-sss branched from v01-13 (origin sss): LibDeps warn gitignore
rm -rf KiTrackMarlin
mkdir KiTrackMarlin
cd KiTrackMarlin
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../KiTrackMarlin
make -j6 install 2>&1 | tee log
cd ..


# acas-sss branched from nightly-20200204 (origin sss): warn
rm -rf MarlinTrkProcessors
mkdir MarlinTrkProcessors
cd MarlinTrkProcessors
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../MarlinTrkProcessors
make -j6 install 2>&1 | tee log
cd ..


# acas-sss branched from v01-03 (origin sss): warn
rm -rf Clupatra
mkdir Clupatra
cd Clupatra
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  ../../Clupatra
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v01-14 (origin sss): warn gitignore cmake
rm -rf ForwardTracking
mkdir ForwardTracking
cd ForwardTracking
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../ForwardTracking
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v00-06 (origin sss): LibDeps warn gitignore
rm -rf MarlinKinfit
mkdir MarlinKinfit
cd MarlinKinfit
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../MarlinKinfit
make -j6 install 2>&1 | tee log
cd ..


# acas-sss branched from v01-28 (origin sss): warn gitignore nullptr
rm -rf MarlinReco
mkdir MarlinReco
cd MarlinReco
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../MarlinReco
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v01-12 (origin v01-12-sss):  prot gitignore jets
rm -rf LCTuple
mkdir LCTuple
cd LCTuple
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../LCTuple
make -j6 install 2>&1|tee log
cd ..


# v03-19-05
#PandoraPFA


# v03-04-01
rm -rf PandoraSDK
mkdir PandoraSDK
cd PandoraSDK
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 -DCMAKE_MODULE_PATH="$ILC_SOFT/PandoraPFA/cmakemodules" ../../PandoraSDK
make -j6 install 2>&1 | tee log
cd ..


# acas-sss branched from v03-01-06 (origin: sss) gitignore
rm -rf LCContent
mkdir LCContent
cd LCContent
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  -DCMAKE_MODULE_PATH="$ILC_SOFT/PandoraPFA/cmakemodules"  ../../LCContent
make -j6 install 2>&1 | tee log
cd ..


# acas-sss branched from v00-11 (origin: sss) warn gitignore plug
rm -rf DDMarlinPandora
mkdir DDMarlinPandora
cd DDMarlinPandora
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  ../../DDMarlinPandora
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v01-00-03 (origin sss): warn gitignore
rm -rf FCalClusterer
mkdir FCalClusterer
cd FCalClusterer
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../FCalClusterer
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v00-08 (origin sss): cmake warn gitignore
rm -rf LCFIVertex
mkdir LCFIVertex
cd LCFIVertex
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../LCFIVertex
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v00-06-06 (origin sss): cmake compile warn gitignore doxy singleton
rm -rf LCFIPlus
mkdir LCFIPlus
cd LCFIPlus
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17 ../../LCFIPlus
make -j6 install 2>&1|tee log
cd ..


# acas-sss branched from v02-00-01 (origin sss): warn gitignore
rm -rf LCPandoraAnalysis
mkdir LCPandoraAnalysis
cd LCPandoraAnalysis
cmake -DCMAKE_INSTALL_PREFIX=$ILC_INST -DCMAKE_CXX_STANDARD=17  ../../LCPandoraAnalysis
make -j6 install 2>&1|tee log
cd ..


rm -rf whizard
mkdir whizard
cd whizard
F77=`which gfortran` ../../whizard-2.8.5/configure --prefix=$ILC_INST 2>&1|tee log-config
make -j6 install OCAMLLIB=$OCAMLLIB HEPMC_INCLUDES="--std=c++17 `HepMC3-config  --cxxflags`"  2>&1|tee log
cd ..


cd ..
cd pythia8303
make distclean
./configure  --prefix=$ILC_INST --with-hepmc2=/cvmfs/sft.cern.ch/lcg/releases/HepMC/2.06.11-d5a39/x86_64-centos7-gcc8-opt
make -j6 install 2>&1|tee log
cd ..

#miniDST: sss branched from nightly-20210102

#edited to add GearXMLFile
#ILDConfig/StandardConfig/production/MarlinStdReco.xml

# Run reco:
#MARLIN_DLL=$ILC_INST/lib/libMarlinDD4hep.so:$ILC_INST/lib/libOverlay.so:$ILC_INST/lib/libMarlinTrkProcessors.so:$ILC_INST/lib/libClupatra.so:$ILC_INST/lib/libForwardTracking.so:$ILC_INST/lib/libMarlinReco.so:$ILC_INST/lib/libLCTuple.so:$ILC_INST/lib/libDDMarlinPandora.so:$ILC_INST/lib/libFCalClusterer.so:$ILC_INST/lib/libLCFIPlus.so:$ILC_INST/lib/libPandoraAnalysis.so Marlin ../ILDConfig/StandardConfig/production/MarlinStdReco.xml --constant.lcgeo_DIR=$lcgeo_DIR --constant.DetectorModel=ILD_l5_o1_v02   --constant.OutputBaseName=zh --global.LCIOInputFiles=zh_SIM.slcio  --global.GearXMLFile=../ILDConfig/StandardConfig/production/Gear/gear_ILD_l5_v02.xml 2>&1|tee log


# Run event display:
#MARLIN_DLL=$ILC_INST/lib/libMarlinDD4hep.so:$ILC_INST/lib/libCEDViewer.so    ced2go -s 1 -d $lcgeo_DIR/ILD/compact/ILD_l5_v02/ILD_l5_v02.xml  zh_REC.slcio 

# Make tuple:
#MARLIN_DLL=$ILC_INST/lib/libMarlinDD4hep.so:$ILC_INST/lib/libLCTuple.so Marlin  --global.LCIOInputFiles=zh_REC.slcio ../ILDConfig/StandardConfig/production/MarlinStdRecoLCTuple.xml 2>&1|tee log



#ddsim --inputFiles run_01/tag_1_pythia8_events.hepmc --outputFile=zd40_500.slcio --compactFile $lcgeo_DIR/SiD/compact/SiD_o2_v03/SiD_o2_v03.xml --steeringFile=../../../SiDPerformance/sid_steer.py 2>&1|tee log

#time Marlin ../SiDPerformance/SiDReconstruction_o2_v03_calib1.xml  --global.LCIOInputFiles=sim/ilc250_eLpR_2f1h_zdark40_500.slcio  --global.GearXMLFile=../SiDPerformance/gear_sid.xml --InitDD4hep.DD4hepXMLFile=$lcgeo_DIR/SiD/compact/SiD_o2_v03/SiD_o2_v03.xml  2>&1|tee log

#--constant.lcgeo_DIR=$lcgeo_DIR  --constant.OutputBaseName=zzd
#--constant.DetectorModel=ILD_l5_o1_v02 
#    <parameter name="GearXMLFile" value="gear_SiD_o2_v03.xml"/>
