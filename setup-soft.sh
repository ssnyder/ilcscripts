export ILC_SOFT=/usatlas/u/snyder/usatlasdata/ilc/soft

export LCG_ROOT=/cvmfs/sft.cern.ch/lcg/views/LCG_98python3/x86_64-centos7-gcc8-opt
. $LCG_ROOT/setup.sh
export PYVERS=python3.7

# We can't have the lcg lib directory on our LD_LIBRARY_PATH.
# That's because lcg also includes dd4hep, which builds root dictionaries.
# ROOT will try to set up dictionaries by walking all directories
# on LD_LIBRARY_PATH, which means that it'll see both versions of dd4hep,
# confusing things greatly.  So remove the generic lcg lib directory
# from our path and instead add the lib directories for the specific
# products that we need.
export LD_LIBRARY_PATH=`$ILC_SOFT/lcghack.py libCore libfreetype libpcre libsqlite3 libtbb libvdt libz libpng libfontconfig libpython3.7m libgsl`

# libglvnd, libglvnd-glx, libglvnd-opengl
export LD_LIBRARY_PATH=/usatlas/u/snyder/MISSING-RPMS7/usr/lib64:$LD_LIBRARY_PATH

export OCAMLLIB=$LCG_ROOT/lib/ocaml

export PATH=$ILC_SOFT/MG5_aMC_v2_8_2/bin:$PATH

export ILC_INST=$ILC_SOFT/inst
export PATH=$ILC_INST/bin:$PATH
export LD_LIBRARY_PATH=$ILC_INST/lib:$LD_LIBRARY_PATH
export PYTHONPATH=$ILC_INST/python:$ILC_INST/lib/$PYVERS/site-packages:$PYTHONPATH
export LCIO=$ILC_INST
export DD4hepINSTALL=$ILC_INST
export lcgeo_DIR=$ILC_SOFT/lcgeo
export DD4HEP=$ILC_SOFT/DD4hep

export CMAKE_PREFIX_PATH=$ILC_INST:$ILC_INST/cmake:$CMAKE_PREFIX_PATH

export MARLIN_DLL=$ILC_INST/lib/libMarlinDD4hep.so:$ILC_INST/lib/libOverlay.so:$ILC_INST/lib/libMarlinTrkProcessors.so:$ILC_INST/lib/libClupatra.so:$ILC_INST/lib/libForwardTracking.so:$ILC_INST/lib/libMarlinReco.so:$ILC_INST/lib/libLCTuple.so:$ILC_INST/lib/libDDMarlinPandora.so:$ILC_INST/lib/libFCalClusterer.so:$ILC_INST/lib/libLCFIPlus.so:$ILC_INST/lib/libPandoraAnalysis.so:$ILC_INST/lib/libCEDViewer.so

export HEPMC_DIR=/cvmfs/sft.cern.ch/lcg/releases/HepMC/2.06.11-d5a39/x86_64-centos7-gcc8-opt
export LD_LIBRARY_PATH=$HEPMC_DIR/lib:$LD_LIBRARY_PATH

export PYTHIA_DIR=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/pythia8/244-989d6/x86_64-centos7-gcc8-opt
export LD_LIBRARY_PATH=$PYTHIA_DIR/lib:$LD_LIBRARY_PATH

