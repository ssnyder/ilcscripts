#!/usr/bin/env python

import sys
import ROOT
from pyLCIO.drivers.Driver import Driver
from array import array
import os


class Dumper( Driver ):
    def __init__ (self, outfname):
        Driver.__init__ (self)
        self.outfname = outfname
        return
    def startOfData (self):
        self.MAXN = 2000
        self.f = ROOT.TFile (self.outfname, 'RECREATE')
        self.t = ROOT.TTree ('MyLCTuple', 'MyLCTuple')
        self.evrun = array ('i', [0])
        self.evevt = array ('i', [0])
        self.evwgt = array ('f', [0])
        self.evsig = array ('f', [0])
        self.nmcp = array ('i', [0])
        self.mcpdg = array ('i', self.MAXN * [0])
        self.mcgst = array ('i', self.MAXN * [0])
        self.mcmox = array ('f', self.MAXN * [0])
        self.mcmoy = array ('f', self.MAXN * [0])
        self.mcmoz = array ('f', self.MAXN * [0])
        self.mcene = array ('f', self.MAXN * [0])
        self.t.Branch ('evrun', self.evrun, 'evrun/I')
        self.t.Branch ('evevt', self.evevt, 'evevt/I')
        self.t.Branch ('evwgt', self.evwgt, 'evwgt/F')
        self.t.Branch ('evsig', self.evsig, 'evsig/F')
        self.t.Branch ('nmcp', self.nmcp, 'nmcp/I')
        self.t.Branch ('mcpdg', self.mcpdg, 'mcpdg[nmcp]/I')
        self.t.Branch ('mcgst', self.mcgst, 'mcgst[nmcp]/I')
        self.t.Branch ('mcmox', self.mcmox, 'mcmox[nmcp]/F')
        self.t.Branch ('mcmoy', self.mcmoy, 'mcmoy[nmcp]/F')
        self.t.Branch ('mcmoz', self.mcmoz, 'mcmoz[nmcp]/F')
        self.t.Branch ('mcene', self.mcene, 'mcene[nmcp]/F')
        return
    def processEvent (self, event):
        mcParticles = event.getMcParticles()
        maxn = min (len (mcParticles), self.MAXN)
        self.evrun[0] = event.getRunNumber()
        self.evevt[0] = event.getEventNumber()
        self.evwgt[0] = event.getWeight()
        self.evsig[0] = 0
        self.nmcp[0] = maxn
        for i in range(maxn):
            p = mcParticles[i]
            v = p.getLorentzVec()
            self.mcpdg[i] = p.getPDG()
            self.mcgst[i] = p.getGeneratorStatus()
            self.mcmox[i] = v.Px()
            self.mcmoy[i] = v.Py()
            self.mcmoz[i] = v.Pz()
            self.mcene[i] = v.Energy()
        self.t.Fill()
        return
    def endOfData (self):
        self.t.Write()
        self.f.Close()
        return


##########################################################################


class GenEvent:
    def __init__ (self, ll):
        self.event_number = int(ll[1])
        self.vertices = {}
        self.particles = {}
        return

    def add_vertex (self, v):
        self.vertices[v.barcode] = v
        return
    def add_particle (self, p):
        self.particles[p.barcode] = p
        return

    def getMcParticles (self):
        return list(self.particles.values())


class GenVertex:
    def __init__ (self, ll):
        self.barcode = int(ll[1])
        self.pdgid = int(ll[2])
        self.x = float(ll[3])
        self.y = float(ll[4])
        self.z = float(ll[5])
        self.norphan = int(ll[7])
        self.parents = []
        self.daughters = []
        return


class GenParticle:
    def __init__ (self, ll):
        self.barcode = int(ll[1])
        self.pdgid = int(ll[2])
        self.px = float(ll[3])
        self.py = float(ll[4])
        self.pz = float(ll[5])
        self.e = float(ll[6])
        self.m = float(ll[7])
        self.status = int(ll[8])
        self.dvertid = int(ll[11])
        self.prod_vert = None
        self.dec_vert = None
        return

    def id (self):
        return self.barcode

    def getPDG (self):
        return self.pdgid

    def getGeneratorStatus (self):
        return self.status

    def getDaughters (self):
        if self.dec_vert != None:
            return self.dec_vert.daughters
        return []

    def getLorentzVec (self):
        return ROOT.TLorentzVector (self.px, self.py, self.pz, self.e)


class HepMCLooper:
    def __init__ (self, fname):
        self.f = open (fname)
        self.line = None
        self.procs = []
        self.skip = 0
        return

    def add (self, proc):
        self.procs.append (proc)
        return

    def skipEvents (self, skip):
        self.skip = skip
        return

    def loop (self, n):
        while n > 0:
            ev = self.read_event()
            if self.skip > 0:
                self.skip -= 1
            else:
                n -= 1
                for p in self.procs:
                    p.processEvent (ev)
        return

    def getline (self):
        if self.line:
            ret = self.line
            self.line = None
            return ret
        ret = self.f.readline()
        if ret != None:
            ret = ret.strip()
        return ret

    def putline (self, l):
        assert (self.line == None)
        self.line = l
        return

    def read_event (self):
        while True:
            l = self.getline()
            if l == None: return None
            ll = l.split()
            if ll and ll[0] == 'E': break

        ev = GenEvent (ll)
        dvertids = {}
        norphan = 0
        v = None
        while True:
            l = self.getline()
            if l == None: break
            ll = l.split()
            if not ll: continue
            if ll[0] == 'E':
                self.putline (l)
                break
            if ll[0] == 'V':
                v = GenVertex (ll)
                plist = dvertids.get (v.barcode, [])
                v.parents = plist
                for p in plist:
                    p.dec_vert = v
                norphan = v.norphan
                ev.add_vertex (v)
            if ll[0] == 'P':
                p = GenParticle (ll)
                dvertids.setdefault (p.dvertid, []).append (p)
                ev.add_particle (p)
                if norphan > 0:
                    norphan -= 1
                    vv = ev.vertices.get (p.dvertid)
                    vv.parents.append (p)
                    p.dec_vert = vv
                else:
                    v.daughters.append (p)
                    p.prod_vert = v

        return ev

##########################################################################
    


def dodump(fname):
    if fname.endswith ('.hepmc'):
        eventLoop = HepMCLooper (fname)
    else:
        from pyLCIO.io.EventLoop import EventLoop
        eventLoop = EventLoop()
        eventLoop.addFile( fname )

    outfname = os.path.splitext(fname)[0] + '.root'

    dumper = Dumper (outfname)
    eventLoop.add( dumper )

    eventLoop.skipEvents( 0 )
    eventLoop.loop( -1 )
    return



#fname= '/usatlas/u/snyder/usatlasdata/ilc/snowass_stdhep/250-TDR_ws/higgs/E250-TDR_ws.Pe1e1h_zz.Gwhizard-1_95.eR.pR.I108056.001.stdhep'
dodump(sys.argv[1])
